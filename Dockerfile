FROM node:18
 
WORKDIR /bot

ENV PATH="./node_modules/.bin:$PATH"

COPY . .

EXPOSE 8080

CMD [ "npm", "ci"]

 
